package com.zuitt.activity;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        // new Phonebook using default constructor
        Phonebook myPhoneBook = new Phonebook();

        // new contact using default constructor
        Contact newContact1 = new Contact();

        // demo setter
        newContact1.setName("Friend1");
        newContact1.setContactNumber("091234567892");
        newContact1.setAddress("Quezon City");

        // new contact using parameterized constructor
        Contact newContact2 = new Contact("Friend2","091234567891","Parañaque");

        // demo getter first contact
        System.out.println(newContact1.getName());
        System.out.println(newContact1.getContactNumber());
        System.out.println(newContact1.getAddress());

        // demo getter second contact
        System.out.println(newContact2.getName());
        System.out.println(newContact2.getContactNumber());
        System.out.println(newContact2.getAddress());

        // Add new contacts to phonebook
        myPhoneBook.addContactToPhonebook(newContact1);
        myPhoneBook.addContactToPhonebook(newContact2);

        // check if phonebook is empty, or not.
        boolean isEmpty = myPhoneBook.contacts.size() > 0? false : true;
        if (isEmpty) {
            System.out.println("Your phonebook has no contacts.");
        } else {
            ArrayList<Contact> myContacts = myPhoneBook.getContacts();
            myContacts.forEach(myContact -> {
                System.out.println("--------------------");
                System.out.println(myContact.getName());
                System.out.println("--------------------");
                System.out.println(myContact.getName() + " has the following registered number: ");
                System.out.println("+"+myContact.getContactNumber());
                System.out.println(myContact.getName() + " has the following registered address: ");
                System.out.println("my home in "+myContact.getAddress());
            });
        }
    }
}
