package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {
    // ArrayList contacts only for Contact objects
    public ArrayList<Contact> contacts = new ArrayList<Contact>();
    // default constructor
    public Phonebook(){}
    //parameterized constructor
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }
    // setter
    public void addContactToPhonebook(Contact newContact){
        this.contacts.add(newContact);
    }
    // getter
    public ArrayList<Contact> getContacts (){ return this.contacts; }
}
