package com.zuitt.example;

// Child class of Animal Class
    // "extend" keyword used to inherit the properties and methods of the parent class.
public class Dog extends Animal {
    // properties
    private String breed;

    // constructor
    public Dog(){
//        super();
        this.breed = "Chihuahua";
    }
    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    // getter and setter ni breed
    public void setBreed(String breed){
        this.breed = breed;
    }

    public String getBreed(){
        return this.breed;
    }

    // method
    public void speak(){
        System.out.println("arf arf");
    }
}
