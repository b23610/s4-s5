package com.zuitt.example;

public class Car {

    // access modifier
        // these are used to restrict the scope of a class, constructor, variable, method, or data member.
        // four types:
            // default - no keyword indicated (accessibility is within the package)
            // private - properties or method only accessible within the class.
            // protected - properties and methods are only accessible by the class of the same package
            // public - properties and method can be accessed from anywhere.

    // class creation
    // four parts of the class creation

    // 1. Properties - characteristics of an object.
    private String name;
    private String brand;
    private int yearOfMake;
    private Driver driver;

    // 2. Constructors - used to create/instantiate an object

    // empty constructor - creates object that doesnt have any arguments/parameters.
    public Car() {
        // Whenever a new car is created it will have a driver named Alejandro
        this.driver = new Driver("Alejandro");
    }

    // parameterized constructor - creates an object with arguments/parameters.
    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Alejandro");
    }

    // 3. Getters and Setters - get and set the values of each property of the object.

    // Getters - use to retrieve the value of the instantiated object.
    public String getName() {
        return this.name;
    }

    public String getBrand() {
        return this.brand;
    }

    public int getYearOfMake() {
        return this.yearOfMake;
    }

    public String getDriverName(){
        return this.driver.getName();
    }

    // Setter - use to change the default values of an instantiated object.
        // can also be modified for added data validation.
    public void setName(String name){
        this.name = name;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake){
        this.yearOfMake = yearOfMake;
    }

    public void setDriverName(String driver){
        this.driver.setName(driver);
    }

    // 4. Methods - function inside an object it can perform (action).
    public void drive() {
        System.out.println("The car is driving");
    }
}
